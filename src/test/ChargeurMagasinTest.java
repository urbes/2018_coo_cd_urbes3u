package test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Test;

import donnees.Magasin;
import XML.ChargeurMagasin;

public class ChargeurMagasinTest {

	@Test
	public void test1_ChargerMagasin_NotFound() {
		ChargeurMagasin cm1 = new ChargeurMagasin("");
		try {
			Magasin m1 = cm1.chargerMagasin();
		} catch (FileNotFoundException e) {
			assertTrue("EZ", true);
		}
	}
	
	@Test
	public void test2_ChargerMagasin_Found() {
		ChargeurMagasin cm2 = new ChargeurMagasin("ok/");
		try {
			Magasin m2 = cm2.chargerMagasin();
		} catch (FileNotFoundException e) {
			assertTrue("FileNotFound", false);
		}
	}

}
